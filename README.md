# Traditional VS. Virtual DOM Explained

This document goes into two examples of traditional web development techniques compared to the Virtual DOM technique.

To explain this, we are using students and a classroom.

- Consider the souls of the students to be their models.
- Consider the bodies of the students to be their corresponding DOM element.
- Consider the traditional method to be how it would be done with a standard backbone.js setup.


## New Student

Let's say you have a classroom with three students, let's call them A, B, and C. Let's take the case where your classroom obtains a new student called D.


### Traditionally

In traditional web development this would work like so:

1. You are told that student D is to be added to your class.
1. You add student D to the end of your class list.
1. You add student D to your class.

> This is optimally performant. It cannot be me made more performant. Specifically:
>
- It has no need to traverse the student list collection.
- It creates a single new model/view.
- It performs a single DOM update.


### Virtual DOM

In React JS:

1. You have a picture of your class.
1. You are told that student D is to be added to your class.
1. You create new class list with student D at the end.
1. You then steal the souls of all your current students and throw them in the bin.
1. You then go through each student in the new list of students, and create souls for each one.
1. You then get all your souls to wait outside the class room, take a picture of them all together, and play spot the difference between it and the old picture of the class you have.
1. In doing so:
	1. You notice that students A, B, and C are the same, so you tell them to inherit the lifeless bodies of students A, B and C.
	1. You notice that student D is new, so you inject student D into the class with a new body.

> This is extremely under performant. It virtually re-renders each student. Specifically:
>
- It traverses the entire student list collection.
- It recreates model/views for every student.
- It performs a single DOM update.


### Virtual DOM Optimised

In React JS with `shouldComponentUpdate` implemented on the `Student` component:

1. You have a picture of your class.
1. You are told that student D is to be added to your class.
1. You create new class list with student D at the end.
1. You then go through each student in the new list of students, and check if they are different than the names you had before. This involves:
	1. If a name is different, you steal the students soul and throw it in a bin, getting a new soul to fill the old body.
	1. If a name is new, you create a new soul, and inject them into the class with a new body.
	1. If a name is missing, you throw out their body with their soul into the garbage.
1. In doing so:
 	1. You notice that students A, B, and C are the same, so you leave their souls live in their current bodies.
	1. You notice that student D is new, so you inject student D into the class with a new body.

> This is quite under performant. It virtually re-renders only the new student. Specifically:
>
- It traverses the entire student list collection.
- It creates a single new model/view.
- It performs a single DOM update.



## Swapping their Seats

Let's say you have a classroom with three students, let's call then A, B, and C. Let's get C to A's seat, and A to take C's seat. So instead of seating like A, B, and C, instead they sit C, B, A.

### Traditionally

In traditional web development this is very complicated and implementation depends on the skill level of the developers, how much time they have, and the libraries that are involved. The easiest implementation would work like so:

1. You are notified that students A and B are meant to switch positions.
1. You dispose of all the students bodies, keeping their souls. (This is a lot of time spent cleaning up the bodies.)
1. For each student soul you still have, you create new bodies for them and inject them into the room. (This is a lot of time spent waiting for everyone to take their seats.)

Doing anything more efficient than this is quite the task.

> This is very under performant. It re-renders every student. Specifically:
>
- It traverses the entire student list collection.
- It modifies no model/views.
- It performs DOM updates for all 3 students.


### Virtual DOM

In React JS:

1. You have a picture of your class.
1. You are notified that students A and B are meant to switch positions.
1. You create a new class list with the updated positions.
1. You then steal the souls of all your current students and throw them in the bin.
1. You then go through each student in the new list of students, and create souls for each one.
1. You then get all your souls to wait outside the class room, take a picture of them all together, and play spot the difference between it and the old picture of the class you have.
1. In doing so:
	1. You notice that students A and C have swapped places:
		1. You inject the new student A's soul into student C's body, which transforms C's body to look like student A
		1. You inject the new student C's soul into student A's body, which transforms A's body to look like student C
	1. You inject the new student B's soul into the old student B's body.

> This is mildly under performant. It virtually re-renders each student. Specifically:
>
- It traverses the entire student list collection.
- It recreates models for all 3 students.
- It performs DOM updates for 2 students.


### Virtual DOM Optimised

In React JS with `shouldComponentUpdate` implemented on the `Student` component:

1. You have a picture of your class.
1. You are notified that students A and B are meant to switch positions.
1. You create a new class list with the updated positions.
1. You then go through each student in the new list of students, and check if they are different than the names you had before. This involves:
	1. If a name is different, you steal the students soul and throw it in a bin, getting a new soul to fill the old body.
	1. If a name is new, you create a new soul, and inject them into the class with a new body.
	1. If a name is missing, you throw out their body with their soul into the garbage.
1. In doing so:
	1. You notice that students A and C have swapped places:
		1. You inject student A's soul into student C's body, which transforms C's body to look like student A
		1. You inject student C's soul into student A's body, which transforms A's body to look like student C
	1. You leave student B's soul intact with student B's body, not needing to make any changes.

> This is very performant. It virtually re-renders 2 students. Specifically:
>
- It traverses the entire student list collection.
- It recreates models for all 2 students.
- It performs DOM updates for 2 students.


## License

Copyright 2015+ Benjamin Lupton <b@lupton.cc> (http://balupton.com). Licensed under the [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).

